<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EjercicioController extends Controller {
    public function cargar(Request $request) {
        $request->validate([
            'archivo' => 'required|mimes:txt|max:2048'
        ]);

        $rutaArchivo = $request->file("archivo")->storeAs("archivos", "matriz.txt", "public");
        $matriz = $this->leerArchivo($rutaArchivo);

        return view("distribuicion", compact("matriz"));
    }

    // <editor-fold defaultstate="collapsed" desc="leer archivo">

    //const CAMINO = 0;
    const PARED = 1;
    const BOMBILLA = 2;
    const ILUMINADO = 3;

    private function leerArchivo(string $ruta): array {
        $matrizTxt = file_get_contents(storage_path() . "/app/public/$ruta", true);
        //se convierte en array, cada línea es una posición
        $matrizArray = explode("\n", trim($matrizTxt));

        $filaImuninada = [];
        $columnaIluminada = [];

        $matrizArrayCount = count($matrizArray);
        for ($i = 0; $i < $matrizArrayCount; $i++) {
            $filaImuninada[$i] = false;

            //se remueve cualquier espacio en blanco
            $matrizArray[$i] = trim($matrizArray[$i]);
            $matrizArray[$i] = preg_replace('/\s+/', "", $matrizArray[$i]);

            $filaCount = strlen($matrizArray[$i]);
            for ($j = 0; $j < $filaCount; $j++) {
                //si no tiene valor en esa columna se inicializa
                if (!array_key_exists($j, $columnaIluminada)) {
                    $columnaIluminada[$j] = false;
                }

                $primeraFila = $i == 0;
                $celdaActual = $matrizArray[$i][$j];
                $esPared = $celdaActual == self::PARED;

                //si es pared, no se hace nada, apagado
                if ($esPared) {
                    $filaImuninada[$i] = false;
                    $columnaIluminada[$j] = false;
                    continue;
                }

                if ($primeraFila) {
                    //si la fila y la columna están iluminadas, no se necesita bombilla
                    if ($filaImuninada[$i]) {
                        $matrizArray[$i][$j] = self::ILUMINADO;
                        continue;
                    }
                } else {
                    //si la fila o la columna está iluminadas, la celda mantiene la iluminación
                    if ($filaImuninada[$i] || $columnaIluminada[$j]) {
                        $matrizArray[$i][$j] = self::ILUMINADO;
                        continue;
                    }
                }
                //si la fila o la columna no están iluminadas (significa que no hay bombilla), se coloca bombilla
                $matrizArray[$i][$j] = self::BOMBILLA;
                $filaImuninada[$i] = true;
                $columnaIluminada[$j] = true;
            }
        }

        return $matrizArray;
    }

    // </editor-fold>
}
