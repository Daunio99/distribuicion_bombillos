<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Distribuición de bombillos</title>

        <link rel="stylesheet" href="{{ asset("css/app.css") }}">
    </head>
    <body class="antialiased">
{{--        {{$hola}}--}}

        <form action="/cargar" method="post">
            <div class="form-group row">
                <label for="inputArchivo" class="col-sm-2 col-form-label">Seleccionar txt</label>
                <div class="col-sm-10">
                    <input type="text" name="archivo" class="form-control" id="inputArchivo">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Cargar</button>
                </div>
            </div>
        </form>
    </body>
</html>
