<?php

$countMatriz = count($matriz);

?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Distribuición de bombillos</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </head>
    <body>

    <div class="p-3">
        <a href="{{ route('inicio') }}" class="btn btn-primary">Inicio</a>
    </div>
    <?php //dd($matriz); ?>

    <div class="p-3">
        <div class="row p-1">
            <div class="col-md-1" id="divSinPared"></div>
            <div class="col-md-11">SIN PARED</div>
        </div>

        <div class="row p-1">
            <div class="col-md-1" id="divPared"></div>
            <div class="col-md-11">PARED</div>
        </div>

        <div class="row p-1">
            <div class="col-md-1" id="divBombillo"></div>
            <div class="col-md-11">BOMBILLO</div>
        </div>
    </div>

    <div class="p-3">
        <?php
        if ($countMatriz > 0) {
            echo "<table>";

            for ($i = 0; $i < $countMatriz; $i++) {
                $countFila = strlen($matriz[$i]);

                echo "<tr>";

                for ($j = 0; $j < $countFila; $j++) {
                    $valor = $matriz[$i][$j];

                    switch ($valor) {
                        case 1: //pared
                            $color = "#000000";
                            break;
                        case 2: //bombilla
                            $color = "#FFFF00";
                            break;
                        default:
                            $color = "#FFFFFF";
                            break;
                    }

                    echo "<td bgcolor='$color'></td>";
                }

                echo "</tr>";
            }

            echo "</table>";
        } else {
            echo "No se pudo procesar el archivo";
        }
        ?>
    </div>
    </body>
</html>

<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    td {
        width: 100px;
        height: 30px;
        text-align: center;
    }

    #divSinPared {
        border-style: solid;
    }

    #divPared {
        background-color: black;
    }

    #divBombillo {
        background-color: #FFFF00;
    }
</style>
